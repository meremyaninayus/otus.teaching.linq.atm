﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using System;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Console
{

    class Program
    {
        const string InputValueEnd = "0";
        const string InputValueGetUserAccounts = "1";
        const string InputValueGetUserAccountsAndHistory = "2";
        const string InputValueGetAllAccountReplenishments = "3";
        const string InputValueGetAllUsersWithBalanceMoreThen = "4";
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            System.Console.WriteLine("Введите логин:");
            var login = System.Console.ReadLine().ToLower();
            System.Console.WriteLine("Введите пароль:");
            var password = System.Console.ReadLine();

            User user = null; ;
            try
            {
                user = atmManager.GetUser(login, password);
            }
            catch (IndexOutOfRangeException ex)
            {
                System.Console.WriteLine(ex.Message);
                return;
            }

            var res = ShowMainWindow(user);
            while (res != InputValueEnd)
            {
                switch (res)
                {
                    //Вывод данных о всех счетах заданного пользователя
                    case InputValueGetUserAccounts:
                        {
                            atmManager.GetUserAccounts(user).Select(
                                x => { System.Console.WriteLine(string.Format("Id счета {0}, сумма {1}", x.Id, x.CashAll)); return x; }).ToList();
                            break;
                        }
                    //Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
                    case InputValueGetUserAccountsAndHistory:
                        {
                            atmManager.GetUserAccountsAndHistory(user).Select(
                                x =>
                                {
                                    System.Console.WriteLine(string.Format("Id счета {0}, дата операции {1}, тип операции {2}, сумма {3}",
                                         x.Item1.Id,
                                         x.Item2.OperationDate,
                                         x.Item2.OperationType,
                                         x.Item2.CashSum)); return x;
                                            }).OrderBy(x => x.Item2.OperationDate).ToList();
                            break;
                        }
                    //Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта
                    case InputValueGetAllAccountReplenishments:
                        {
                            atmManager.GetAllAccountReplenishments().Select(
                                x =>
                                {
                                    System.Console.WriteLine(string.Format("Фамилия {0}, дата операции {1}, сумма {2}",
                                 x.Item1.SurName,
                                 x.Item2.OperationDate,
                                 x.Item2.CashSum)); return x;
                                }).OrderBy(x => x.Item2.OperationDate).ToList();
                            break;
                        }
                    //Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
                    case InputValueGetAllUsersWithBalanceMoreThen:
                        {
                            var sumStr = System.Console.ReadLine();
                            if (int.TryParse(sumStr, out int sum))
                            {
                                var count = atmManager.GetAllUsersWithBalanceMoreThen(sum).Distinct().Select(
                                    x =>
                                    {
                                        System.Console.WriteLine(string.Format("Фамилия {0}",
                                     x.SurName)); return x;
                                    }).Count();
                                if (count == 0)
                                {
                                    System.Console.WriteLine("Нет таких");
                                }
                            }
                            else
                            {
                                System.Console.WriteLine("Введено некорректное значение суммы");
                            }
                            break;
                        }
                    //Введено некорректное значение
                    default:
                        {
                            System.Console.WriteLine("Введено некорректное значение");
                            break;
                        }
                }
                System.Console.ReadKey();
                res = ShowMainWindow(user);
            }
            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        private static string ShowMainWindow(User user)
        {
            System.Console.Clear();
            System.Console.WriteLine(string.Format("Добрый день! {0} {1}", user.FirstName, user.MiddleName));
            System.Console.WriteLine("Для получения данных по счетам нажмите введите 1");
            System.Console.WriteLine("Для получения детализированных данных по счетам нажмите введите 2");
            System.Console.WriteLine("Для получения данных о всех пополнениях введите  3");
            System.Console.WriteLine("Для получения данных о пользователях с суммой больше заданной на счетах введите 4, а в следующей строке введите граничную сумму");
            System.Console.WriteLine("Для завершения работы нажмите 0");
            return System.Console.ReadLine();
        }


        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}