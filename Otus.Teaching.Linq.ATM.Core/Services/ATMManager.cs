﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //Вывод информации о заданном аккаунте по логину и паролю
        public User GetUser(string login, string password)
        {
            var user = Users.Where(x => x.Login == login && x.Password == password).FirstOrDefault();
            if (user == null)
            {
                throw new InvalidOperationException("Пользователь с таким логином и паролем не найден");
            }
            return user;
        }

        //Вывод данных о всех счетах заданного пользователя
        public IEnumerable<Account> GetUserAccounts(User user)
        {
            return Accounts.Where(x => x.UserId == user.Id).ToList();
        }

        //Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
        public IEnumerable<(Account, OperationsHistory)> GetUserAccountsAndHistory(User user)
        {
            return from Account in Accounts
                   join OperationsHistory in History on Account.Id equals OperationsHistory.AccountId
                   select (Account, OperationsHistory);
        }

        //Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта
        public IEnumerable<(User, OperationsHistory)> GetAllAccountReplenishments()
        {
            return from User in Users
                   join Account in Accounts on User.Id equals Account.UserId
                   join OperationsHistory in History on Account.Id equals OperationsHistory.AccountId
                   where OperationsHistory.OperationType == OperationType.InputCash
                   select (User, OperationsHistory);
        }

        //Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
        public IEnumerable<User> GetAllUsersWithBalanceMoreThen(decimal sum)
        {
            return from User in Users
                   join Account in Accounts on User.Id equals Account.UserId
                   where Account.CashAll > sum
                   select User;
        }
    }
}